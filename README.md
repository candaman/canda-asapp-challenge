# Canda Asapp Challenge

Coding challenge for Asapp. This is a split screen chat. On Rob's window, Laura will write to Rob, and viceversa.

I hope you like it!

Live Demo https://asappchallengecanda.cf

## Setup

```
  npm install
  npm start
```
