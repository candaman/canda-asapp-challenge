import React from 'react';
import moment from 'moment';
import styled, { createGlobalStyle } from 'styled-components';

import ChatWindow from './Chat/ChatWindow';

const PEOPLE = {
  laura: {
    name: 'Laura Alden',
    id: 'laura',
    imageSrc: '/photos/laura.jpg'
  },
  rob: {
    name: 'Rob Watson',
    id: 'rob',
    imageSrc: '/photos/rob.jpg'
  }
};

const CHAT_WINDOWS = [{
  sender: PEOPLE.laura,
  receiver: PEOPLE.rob
}, {
  sender: PEOPLE.rob,
  receiver: PEOPLE.laura
}];

const BodyStyles = createGlobalStyle`
  html {
    padding: 0;
    margin: 0;
    width: 100%;
    height: 100%;
  }

  body {
    background: #222222;
    padding: 0;
    margin: 0;
    width: 100%;
    height: 100%;
  }

  #root {
    width: 100%;
    height: 100%;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media (min-width: 700px) {
    flex-direction: row;
    height: 100%
  }
`;

// In real life we would change this 2 methods to send and receive data through network

// Toggles the isTyping flag at the receivers chat state
const toggleIsTypingState = (chatStates, senderId, receiverId, isOtherUserTyping) => ({
  ...chatStates,
  [receiverId]: {
    ...chatStates[receiverId],
    [senderId]: {
      ...chatStates[receiverId][senderId],
      isOtherUserTyping
    }
  }
});

// Adds a message to a chat state
// It's called twice for each message:
//  - once for the sender chat state
//  - once for the receiver chat state
const addMessageToState = (chatStates, senderId, receiverId, content, isOwn) => ({
  ...chatStates,
  [senderId]: {
    ...chatStates[senderId],
    [receiverId]: {
      ...chatStates[senderId][receiverId],
      messages: [...chatStates[senderId][receiverId].messages, {
        content,
        isOwn,
        date: moment().valueOf()
      }]
    }
  }
});

class App extends React.Component {
  state = {
    // chatStates[sender][receiver]: { messages: [], isOtherUserTyping }
    chatStates: CHAT_WINDOWS.reduce((chatStates, chat) => {
      const newChatStates = {
        ...chatStates,
        [chat.sender.id]: {
          ...chatStates[chat.sender.id],
          [chat.receiver.id]: {
            messages: [],
            isOtherUserTyping: false
          }
        }
      };
      return newChatStates;
    }, {})
  }

  sendMessage = (senderId, receiverId, content) => {
    this.setState(({ chatStates }) => {
      let newChatStates = addMessageToState(chatStates, senderId, receiverId, content, true);
      newChatStates = addMessageToState(newChatStates, receiverId, senderId, content, false);

      return { chatStates: newChatStates };
    });
  }

  onStartTyping = (senderId, receiverId) => {
    this.setState(({ chatStates }) => ({
      chatStates: toggleIsTypingState(chatStates, senderId, receiverId, true)
    }));
  }

  onStopTyping = (senderId, receiverId) => {
    this.setState(({ chatStates }) => ({
      chatStates: toggleIsTypingState(chatStates, senderId, receiverId, false)
    }));
  }

  render() {
    const { chatStates } = this.state;
    return (
      <Wrapper>
        <BodyStyles />

        {CHAT_WINDOWS.map((chat) => {
          const chatState = chatStates[chat.sender.id][chat.receiver.id];

          return (
            <ChatWindow
              key={`${chat.sender.id}${chat.receiver.id}`}
              currentUser={chat.sender}
              otherUser={chat.receiver}
              messages={chatState.messages}
              isOtherUserTyping={chatState.isOtherUserTyping}
              onStartTyping={this.onStartTyping}
              onStopTyping={this.onStopTyping}
              sendMessage={this.sendMessage}
            />
          );
        })}
      </Wrapper>
    );
  }
}

export default App;
