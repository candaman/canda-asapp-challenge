import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import moment from 'moment';

const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 400px;
  overflow: auto;

  @media (min-width: 700px) {
    flex-grow: 1;
    height: auto
  }
`;

const Content = styled.p`
  white-space: pre-wrap;
  align-self: flex-start;
  font-size: 12px;
  line-height: 18px;
  margin: 0;
  overflow-wrap: break-word;
  word-wrap: break-word;
  max-width: 100%;
`;

const Date = styled.span`
  align-self: flex-end;
  font-size: 8px;
  padding-top: 5px;
`;

const ownMessageStyles = css`
  align-self: flex-end;
  background: #1588FD;
  color: #ffffff;
  ${Date} {
    color: #ddd;
  }
`;

const incomingMessageStyles = css`
  align-self: flex-start;
  background: #E6E5EB;
  color: #000000;
  ${Date} {
    color: #5b5b5b;
  }
`;

const MessageWrapper = styled.div`
  ${props => (props.isOwn ? ownMessageStyles : incomingMessageStyles)}
  display: flex;
  flex-direction: column;
  padding: 10px;
  margin: 10px;
  border-radius: 10px;
  max-width: calc(100% - 40px);
  flex-shrink: 0;

  animation: openMessage 0.3s;
  @keyframes openMessage {
    0% {
      transform-origin: bottom;
      transform: rotateX(-90deg);
      opacity: 0;
    }
    100% {
      transform-origin: bottom;
      transform: rotateX(0);
      opacity: 1;
    }
  }
`;

const TypingIcon = styled.img`
  height: 20px;
  padding: 0 5px;
`;

class MessageList extends React.Component {
  listWrapperElement = React.createRef();

  componentDidUpdate = (prevProps) => {
    const previousMessages = prevProps.messages;
    const wasOtherUserTyping = prevProps.isOtherUserTyping;
    const { messages, isOtherUserTyping } = this.props;

    const element = this.listWrapperElement.current;

    if (previousMessages !== messages) {
      element.scrollTop = element.scrollHeight;
    }

    if (isOtherUserTyping && !wasOtherUserTyping) {
      element.scrollTop = element.scrollHeight;
    }
  }

  render() {
    const { messages, isOtherUserTyping } = this.props;

    return (
      <ListWrapper ref={this.listWrapperElement}>
        {messages.map(message => (
          <MessageWrapper isOwn={message.isOwn} key={message.date}>
            <Content>{message.content}</Content>
            <Date>{moment(message.date).calendar()}</Date>
          </MessageWrapper>
        ))}
        {isOtherUserTyping && (
          <MessageWrapper isOwn={false}>
            <TypingIcon src="/icons/typing.gif" alt="..." />
          </MessageWrapper>
        )}
      </ListWrapper>
    );
  }
}

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string.isRequired,
    date: PropTypes.number.isRequired,
    isOwn: PropTypes.bool.isRequired
  })).isRequired,
  isOtherUserTyping: PropTypes.bool.isRequired
};

export default MessageList;
