import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TextareaAutosize from 'react-textarea-autosize';
import debounce from 'debounce';

import userShape from '../shapes/userShape';

const Wrapper = styled.div`
  display: flex;
  padding: 10px;
  background: #F9F9F9;
  position: relative;
`;

const StyledTextArea = styled(TextareaAutosize)`
  border: none;
  overflow: auto;
  outline: none;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 1px 1px inset;
  resize: none;
  flex-grow: 1;
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  min-height: 15px;
  vertical-align: middle;
  display: inline-block;
  font-size: 12px;
  line-height: 18px;
  padding: 10px;
  padding-right: 45px;
  border-radius: 8px;
`;

const SendButton = styled.img`
  width: 26px;
  height: 26px;
  position: absolute;
  right: 20px;
  top: 50%;
  transform: translateY(-50%);
  cursor: pointer;
`;

class MessageInput extends React.Component {
  state = {
    inputText: ''
  }

  debouncedStopTyping = debounce(this.props.onStopTyping, 3000);

  componentDidUpdate = (prevProps) => {
    if (prevProps.onStopTyping !== this.props.onStopTyping) {
      this.debouncedStopTyping = debounce(this.props.onStopTyping, 3000);
    }
  }

  onTextInput = (event) => {
    this.props.onStartTyping(this.props.currentUser.id, this.props.otherUser.id);
    this.debouncedStopTyping(this.props.currentUser.id, this.props.otherUser.id);

    this.setState({
      inputText: event.target.value
    });
  }

  onInputKeyPress = (event) => {
    if (event.charCode === 13 && !event.shiftKey) {
      event.preventDefault();
      this.sendMessage();
    }
  }

  sendMessage = () => {
    const {
      sendMessage,
      onStopTyping,
      currentUser,
      otherUser
    } = this.props;
    const { inputText } = this.state;

    if (!inputText) {
      return;
    }

    sendMessage(currentUser.id, otherUser.id, inputText);
    onStopTyping(currentUser.id, otherUser.id);

    this.setState({
      inputText: ''
    });
  }

  render() {
    const { inputText } = this.state;
    return (
      <Wrapper>
        <StyledTextArea
          rows={1}
          value={inputText}
          onChange={this.onTextInput}
          onKeyPress={this.onInputKeyPress}
          placeholder="Message"
        />
        <SendButton src="/icons/send.png" onClick={this.sendMessage} />
      </Wrapper>
    );
  }
}

MessageInput.propTypes = {
  currentUser: PropTypes.shape(userShape).isRequired,
  otherUser: PropTypes.shape(userShape).isRequired,
  sendMessage: PropTypes.func.isRequired,
  onStartTyping: PropTypes.func.isRequired,
  onStopTyping: PropTypes.func.isRequired
};

export default MessageInput;
