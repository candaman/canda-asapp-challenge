import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import userShape from '../shapes/userShape';

const Wrapper = styled.div`
  background: #F9F9F9;
  border-bottom: 1px solid #ECECEC;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;
`;

const Avatar = styled.div`
  background-image: url(${props => props.imageSrc});
  background-size: cover;
  background-position: top center;
  border-radius: 50%;
  width: 30px;
  height: 30px;
`;

const Label = styled.span`
  padding-top: 5px;
  font-weight: bold;
  font-size: 14px;
  color: #333;
`;

const Header = ({ otherUser }) => (
  <Wrapper>
    <Avatar imageSrc={otherUser.imageSrc} />
    <Label>{otherUser.name}</Label>
  </Wrapper>
);

Header.propTypes = {
  otherUser: PropTypes.shape(userShape).isRequired
};

export default Header;
