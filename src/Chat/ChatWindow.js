import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import userShape from '../shapes/userShape';

import MessageInput from './MessageInput';
import Header from './Header';
import MessageList from './MessageList';

const Wrapper = styled.div`
  margin: 10px;
  background: white;
  border-radius: 2px;
  overflow: hidden;
  display: flex;
  flex-direction: column;

  @media (min-width: 700px) {
    flex-grow: 1;
  }
  position: relative;
`;

const ChatWindow = ({
  currentUser,
  otherUser,
  messages,
  isOtherUserTyping,
  onStartTyping,
  onStopTyping,
  sendMessage
}) => (
  <Wrapper>
    <Header otherUser={otherUser} />
    <MessageList
      messages={messages}
      isOtherUserTyping={isOtherUserTyping}
    />
    {/* {isOtherUserTyping && (
      <IsTyping>
        {otherUser.name} is typing...
      </IsTyping>
    )} */}
    <MessageInput
      onStartTyping={onStartTyping}
      onStopTyping={onStopTyping}
      sendMessage={sendMessage}
      currentUser={currentUser}
      otherUser={otherUser}
    />
  </Wrapper>
);

ChatWindow.propTypes = {
  currentUser: PropTypes.shape(userShape).isRequired,
  otherUser: PropTypes.shape(userShape).isRequired,
  messages: PropTypes.arrayOf(PropTypes.shape({
    content: PropTypes.string,
    date: PropTypes.number,
    isOwn: PropTypes.bool
  })),
  isOtherUserTyping: PropTypes.bool,
  onStartTyping: PropTypes.func.isRequired,
  onStopTyping: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired
};

ChatWindow.defaultProps = {
  messages: [],
  isOtherUserTyping: false
};

export default ChatWindow;
